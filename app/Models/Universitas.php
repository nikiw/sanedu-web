<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Universitas extends Model {
    use SoftDeletes;
    protected $table        = 'tbl_universitas';
    protected $primaryKey   = 'id';
    protected $keyType      = 'string';
    public $incrementing    = false;
    protected $dates        = ['deleted_at'];

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {
            // $data->tiket()->delete();
        });
    }

    //RELATION table
    public function jurusan() {
        return $this->hasMany('App\Models\Jurusan', 'id_universitas');
    }
    public function getNamaAttribute($value) {
        return strtoupper($value);
    }
}
